<html lang="jp">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="3NtJnriCjQHQ8JegZyefO6sGubFoSDSQCCkYn5lq">

        <title>TASCROP</title>


        <link href="http://localhost:8000/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://localhost:8000/plugins/select2/css/select2.min.css" rel="stylesheet">
        <link href="http://localhost:8000/sass/styles.css" rel="stylesheet">
        <link href="http://localhost:8000/css/custom.css" rel="stylesheet">
        <link href="http://localhost:8000/sass/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link href="http://localhost:8000/sass/select2-type_use.css" rel="stylesheet">
        <link href="http://localhost:8000/plugins/date-time/jquery.timepicker.min.css" rel="stylesheet">
        <link href="http://localhost:8000/plugins/tribute-master/dist/tribute.css" rel="stylesheet">
        <link href="http://localhost:8000/plugins/suneditor/assets/css/suneditor.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://localhost:8000/plugins/date-time/bootstrap-datetimepicker.css">
        <link href="http://localhost:8000/plugins/select2/css/select2.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/app.css">

        <style>
            .sun-editor .se-list-layer {
                width: 300px;
            }

            .sun-editor .se-list-layer span {
                margin: 1px;
                padding-top: 1px;
            }

            .project-search .project-wrap .content-dashboard div p {
                font-size: unset;
            }

        </style>
        <style>
            .select2-container--default .select2-selection--single:focus-visible {
                outline: none;
                border-color: #86b7fe !important;
                box-shadow: 0 0 0 0.25rem rgb(13 110 253 / 25%);
            }

            .attachment .attachment-list .attachment-item {
                display: flex;
                align-items: center;
                margin-top: 12px;
            }

            .attachment-item p.name-file {
                margin-right: auto;
                text-decoration: underline;
            }

            .attachment .attachment-list .attachment-item .close {
                cursor: pointer;
                width: 16px;
                height: 16px;
            }

        </style>

        <style>
            /*
            /*---- set background -----------*/
            /* set background header title by background_header_title_color_code */
            #body-row.skill-green .content .setting-profile .profile-left .setting-config__title,
            #body-row.skill-green .content .setting-profile .profile-right .notify-config {
                background-color: #B9DADF

            }
            /* set background  by color_code */
            #body-row.skill-green .main-navigation,
            #body-row.skill-green .select-all__btn.active,
            #body-row.skill-green .select2-container--default .select2-dropdown .select2-results .select2-results__option--selected {
                background-color: #14746f

            }

            /* background  by selected_sub_color_code */
            #body-row.skill-green .sub-active.active,
            #body-row.skill-green .manage-users .manage-user-wp .manage-user-header .manage-user-header_number-licenses,
            #body-row.skill-green .content-payment-manager .payment-manager-page .wp-body-payment .pg-body-content-left .wp-info-payment .payment-date__text,
            #body-row.skill-green .content-payment-manager .payment-manager-page .wp-body-payment .pg-body-content-left .wp-info-payment .number-licenses-use__text,
            #body-row.skill-green .sub-menu:hover {
                background-color: #EAF2F0

            }

            /* background  by icon_active_color_code */
            #body-row.skill-green .edit-project .wp-box-title .btn-gr .complete__btn,
            #body-row.skill-green .project-page .project-groups .project-wrap .project-task-wrap .add-task,
            #body-row.skill-green .content-wrapper.project-search .project-wrap .list-project .btn-wrap .active,
            #body-row.skill-green .menu-setting__hover:hover,
            #body-row.skill-green .tab-content .dropdown-filter-btn__hover:hover,
            #body-row.skill-green  .select2-dropdown .select2-results li:hover,
            #body-row.skill-green .project-page .add-project-notification .btn-wrap button:hover,
            #body-row.skill-green  .project-information .wrap-main-project .dropdown-role .options li:hover,
            #body-row.skill-green  .form-search .result-search .info-user:hover {
                background-color: #56ab91

            }

            #body-row.skill-green .btn-action-wrap .accept__btn,
            #body-row.skill-green .content .setting-profile .setting-account__btn,
            #body-row.skill-green .manage-user-wp .sub__btn,
            .skill-green#body-row  .modal-dialog .modal-content .btn-wrap button.copy__btn , .skill-green#body-row .btn-wrap button.copy__btn,
            #body-row.skill-green .wp-corporate-inf .corporate-info-content .save__btn,
            #body-row.skill-green .manager-user-table__btn .btn-save__change,
            #body-row.skill-green .project-information .btn-gr .save__btn,
            #body-row.skill-green .payment-manager-page .wp-body-payment .pg-body-content-left .wp-credit-card .form .btn-save,
            #body-row.skill-green #showConfirmDeleteComplete button.btn.btn__cancellation-complete,
            #body-row.skill-green #showManagerPayMent .modal-dialog .modal-content .modal-body-payment-page .btn-wrap .btn__cancellation,
            #body-row.skill-green #showConfirmDeleteUser .modal-dialog .modal-content .modal-body-show-confirm-delete-user .btn-wrap .btn__cancellation-complete,
            #body-row.skill-green #showServiceGoogleChat .modal-body-service-gg-chat .btn-commom,
            #body-row.skill-green #showServiceGoogleSchedule .modal-body-service-gg-schedule .btn-commom,
            #body-row.skill-green #showServiceSlack .modal-dialog-service-slack .btn-commom,
            #body-row.skill-green #changePassModal .modal-dialog .modal-content .modal-footer__pass .btn.sub__btn,
            #body-row.skill-green #editScreenModal .modal-dialog .modal-content .save__btn,
            #body-row.skill-green #trashTaskRestoreModal .modal-dialog .modal-content .btn-wrap .su__btn,
            #body-row.skill-green #trashTaskDeleteModal .modal-dialog .modal-content .btn-wrap .su__btn,
            #body-row.skill-green #trashProjectRestorationModal .modal-dialog .modal-content .btn-wrap .su__btn,
            #body-row.skill-green #trashProjectDeletionModal .modal-dialog .modal-content .btn-wrap .su__btn,
            #body-row.skill-green #trashGroupRestoreModal .modal-dialog .modal-content .btn-wrap .su__btn,
            #body-row.skill-green #trashGroupDeleteModal .modal-dialog .modal-content .btn-wrap .su__btn,
            #body-row.skill-green #trashFileRecoveryModal .modal-dialog .modal-content .btn-wrap .su__btn,
            #body-row.skill-green #trashFileDeleteConfirmModal .modal-dialog .modal-content .btn-wrap .su__btn,
            #body-row.skill-green .content-wrapper.member-search .project-wrap .list-project .search-result .dropdown-menu.dropdown-menu-filter .dropdown-filter .tab-content .project-authority-tab .authority__btn.button-tab-active,
            #body-row.skill-green  .nav-tab-border .wrap-nav-tb .nav-tabs li .tab-dot,
            #body-row.skill-green .content-wrapper.project-task-search .project-task-wrap .list-task-project .btn-wrap .active,
            #body-row.skill-green .content-wrapper.project-task-search .project-task-wrap .content-dashboard .project-task-search-detail .copy__btn,
            #body-row.skill-green .content-wrapper.project-task-search .project-task-wrap .list-task-project .search-result .dropdown.filter .dropdown-menu-filter .dropdown-filter .tab-content .btn.active,
            #body-row.skill-green #showContact .modal-dialog .modal-content .btn-wrap .contact__btn,
            #body-row.skill-green .modal-dialog .btn-wrap .send__btn,
            #body-row.skill-green .content-wrapper.project-search .project-search-by-keyword .dropdown.filter .dropdown-filter .tab-content .btn.active,
            #body-row.skill-green #showTaskCopySettingModal .modal-dialog .modal-content .task-copy__btn,
            #body-row.skill-green #showGroupSetting .modal-dialog .modal-content .modal-footer__btn .btn-action-wrap .save__btn,
            #body-row.skill-green .updateTaskGroupModal .modal-dialog .modal-content .modal-footer__btn .btn-action-wrap .save__btn,
            #body-row.skill-green #showGroupSetting .modal-dialog .modal-content .modal-footer__btn .btn-action-wrap .save__btn,
            #body-row.skill-green #groupProjectCopyModal .modal-dialog .modal-content .btn-wrap .copy__btn,
            #body-row.skill-green #saveTemplateModal .modal-dialog .modal-content .btn-wrap .copy__btn,
            #body-row.skill-green #showExportProject .modal-dialog.modal-dialog-character .btn-wrap .copy__btn,
            #body-row.skill-green #showEditProjectNotificationModal .modal-dialog .modal-content .btn-wrap .register__btn,
            #body-row.skill-green .dropdown-menu.dropdown-menu-filter .dropdown-filter .tab-content .btn.active,
            #body-row.skill-green #showRegisterProjectModal .modal-dialog .modal-content .btn-wrap .register__btn,
            #body-row.skill-green .add-license .license-wrap .btn-wrap .confirm__btn,
            #body-row.skill-green .add-license .license-wrap .btn-wrap .del__btn,
            #body-row.skill-green .delete-license .license-wrap .btn-wrap .confirm__btn,
            #body-row.skill-green .delete-license .license-wrap .btn-wrap .del__btn,
            #body-row.skill-green .confirm-delete-license .license-wrap .btn-wrap .confirm__btn,
            #body-row.skill-green .confirm-delete-license .license-wrap .btn-wrap .del__btn,
            #body-row.skill-green .confirm-payment-method .license-wrap .btn-wrap .buy__btn,
            #body-row.skill-green .sidebar-menu.menu-active,
            #body-row.skill-green #showGroupSetting .modal-dialog .modal-content .modal-body-group__settings .display-color-wrap .project-wrap-preview .project-task-wrap .add-task,
            #body-row.skill-green .project-task.active:before,
            #body-row.skill-green .project-wrap.active:before,
            #body-row.skill-green .list-user-group .tr.active:before,
            #body-row.skill-green .sub-active.active:before,
            #body-row.skill-green .project-progress .progress.progress-custom .progress-bar-custom,
            #body-row.skill-green .sidebar-collapsed .sidebar-top,
            .skill-green#body-row input[type="checkbox"]:checked,
            .skill-green#body-row input[type="radio"].form-check-input:checked,
            .skill-green#body-row .tab-content .dropdown-filter-btn__hover.active,
            #body-row.skill-green .project-page.project-page-list-all .display-switch .btn-switch .active,
            #body-row.skill-green .list-user .user-item-wrap .item:hover,
            #body-row.skill-green .inner-d010:hover,
            #body-row.skill-green .modal-dialog .modal-content .btn-action-wrap .accept__btn,
            #body-row.skill-green .manage-use-table .tbody .tr .td-icon .delete__btn .dropdown-menu .inner:hover {
                background-color: #358f80
            }

            /* background button_color_code */
            #body-row.skill-green .content .setting-profile .profile-left .profile-change-pass__btn,
            #body-row.skill-green .select-all__btn {
                background-color: #E1E9E7

            }

            /*---- set border -----------*/
            /* border button_color_code */
            #body-row.skill-green #sidebar-container.sidebar-collapsed,
            #body-row.skill-green .wrap-main-content,
            .skill-green#body-row input[type="checkbox"]:checked, .skill-green#body-row input[type="radio"].form-check-input:checked {
                border-color: #358f80

            }

            #body-row.skill-green .nav-tab-header #nav-tab .nav-link:hover {
                border-bottom: 4px solid#14746f

            }

            #body-row.skill-green .content-tab-1 .project-wrap .task-registration .project-task-registration-tab .nav-tab-header #nav-tab .nav-link:hover {
                border-bottom: 1px solid#14746f

            }

            #body-row.skill-green .content-tab-3 .project-wrap .task-registration .project-task-registration-tab .nav-tab-header #nav-tab .nav-link:hover {
                border-bottom: 1px solid#14746f

            }

            #body-row.skill-green .sub-task-registration .project-task-registration-tab .nav-tab-header #nav-tab .nav-link:hover, .skill-green .task-registration .project-task-registration-tab .nav-tab-header #nav-tab .nav-link:hover {
                border-bottom: 1px solid#56ab91

            }

            #body-row.skill-green .nav-tab-header #nav-tab .nav-link.active {
                border-bottom: 4px solid#14746f

            }

            #body-row.skill-green .task-registration .project-task-registration-tab .nav-tab-header #nav-tab .nav-link.active,
            #body-row.skill-green .sub-task-registration .project-task-registration-tab .nav-tab-header #nav-tab .nav-link.active {
                border-bottom: 1px solid#56ab91

            }

            #body-row.skill-green .sidebar-menu .div-wrap-item .project-ceate-menu-content .project-copy-menu-item:hover, .sidebar-menu .div-wrap-item .project-ceate-menu-content .project-ceate-menu-item:hover {
                background-color: #56ab91

            }

            #body-row.skill-green .content .setting-profile .profile-left,
            #body-row.skill-green .content .setting-profile .profile-right {
                border-color: #B9DADF

            }

            /*---- set fill -----------*/

            /* fill color_code */
            #body-row.skill-green .icon-content-svg,
            #body-row.skill-green .avatar__icon,
            #body-row.skill-green .note__icon,
            #body-row.skill-green .weigh__icon,
            #body-row.skill-green .bell__icon,
            #body-row.skill-green .next__icon.active {
                fill: #14746f

            }


            #body-row.skill-green .content-wrapper.company-info .company-info-wrap .company-info-header .company-icon-wrap .company-question__icon,
            #body-row.skill-green .bc-subtask .bc-subtask-item-icon,
            #body-row.skill-green .my-portal .avatar__icon,
            #body-row.skill-green svg.icon-quection__js path,
            #body-row.skill-green svg.icon-question,
            #body-row.skill-green .user-icon__pass,
            #body-row.skill-green .bc-sub-pro-wrap .bc-subtask .bc-subtask-item-icon,
            #body-row.skill-green svg.icon-quection__js > path, .skill-green svg.icon-question > path,
            #body-row.skill-green .bc-like__icon.active {
                fill: #358f80

            }

            #body-row.skill-green .project-page .project-groups .avatar__icon,
            #body-row.skill-green .add-user__icon,
            #body-row.skill-green .add-project__icon,
            #body-row.skill-green .add-task-group__icon,
            .icon-add-breakdown-list,
            #body-row.skill-green .manager-user-table__icon .add-group__icon,
            #body-row.skill-green .project-information .wrap-main-project .wp-content-member .add-member .add-member__icon,
            #body-row.skill-green .project-information .wrap-main-project .wrap-guest .add-guest__icon,
            #body-row.skill-green #editScreenModal .modal-dialog .modal-content .modal-body-content .wp-add-role .add-role__icon {
                fill: #56ab91

            }

            /* ----- set Color -----*/
            #body-row.skill-green .content .setting-profile .profile-left .setting-config__title,
            #body-row.skill-green .content .setting-profile .profile-left .profile-change-pass__btn,
            #body-row.skill-green .content-payment-manager .payment-manager-page .wp-body-payment .pg-body-content-left .wp-info-payment .payment-date__text,
            #body-row.skill-green .content-payment-manager .payment-manager-page .wp-body-payment .pg-body-content-left .wp-info-payment .number-licenses-use__text,
            #body-row.skill-green .content-payment-manager .payment-manager-page .wp-body-payment .pg-body-content-left .wp-info-payment .number-licenses-use__text span,
            #body-row.skill-green .manage-users .manage-user-wp .manage-user-header .group-number-license .number-licenses-upurchased__text,
            #body-row.skill-green .manage-users .manage-user-wp .manage-user-header .group-number-license .number-licenses-use__text,
            #body-row.skill-green .content .setting-profile .profile-right .notify-config {
                color: #14746f  !important;
            }

            .skill-green .select-all__btn {
                color: #14746f  !important;
            }
            #body-row.skill-green .select-all__btn.active {
                color: #F8F9FA !important;
            }

            /* stroke icon_active_color_code */
            #body-row.skill-green .success__icon-active,
            #body-row.skill-green .checked-sucess__icon {
                stroke: #56ab91
            }

            #body-row.skill-green #showContactResponseMessageAccept .modal-dialog .modal-content .modal-header-contact-response .nav-tabs .nav-link.active,
            #body-row.skill-green #showContactResponseMessageAccept .modal-dialog .modal-content .modal-header-contact-response .nav-tabs .nav-link:hover{
                border-color: #56ab91
            }

        </style>
        <!--XRAY END 1-->
        <style type="text/css">/* Chart.js */
            @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style><script type="text/javascript" charset="UTF-8" src="https://www.gstatic.com/charts/51/loader.js"></script><link id="load-css-0" rel="stylesheet" type="text/css" href="https://www.gstatic.com/charts/51/css/core/tooltip.css"><link id="load-css-1" rel="stylesheet" type="text/css" href="https://www.gstatic.com/charts/51/css/util/util.css"><script type="text/javascript" charset="UTF-8" src="https://www.gstatic.com/charts/51/js/jsapi_compiled_default_module.js"></script><script type="text/javascript" charset="UTF-8" src="https://www.gstatic.com/charts/51/js/jsapi_compiled_graphics_module.js"></script><script type="text/javascript" charset="UTF-8" src="https://www.gstatic.com/charts/51/js/jsapi_compiled_ui_module.js"></script><script type="text/javascript" charset="UTF-8" src="https://www.gstatic.com/charts/51/js/jsapi_compiled_corechart_module.js"></script>
    </head>

    <body class="skill-green" id="body-row" style="overflow: hidden; padding-right: 0px;">

        <nav class="navbar navbar-expand-md main-navigation main-nav-header-js" data-user-id="S18p4cUO-P0au-IKaa-HHHq-rzMAao41Zuno" aria-label="Fourth navbar" style="background: #14746f">
            <div class="container-fluid">
                <a class="navbar-brand nav-site-title" href="/my-portal/my-task">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="211.022" height="36" viewBox="0 0 211.022 36">
                        <defs>
                            <linearGradient id="linear-gradient" x1="0.25" y1="0.933" x2="0.747" y2="0.071" gradientUnits="objectBoundingBox">
                                <stop offset="0" stop-color="#fff" stop-opacity="0.302"></stop>
                                <stop offset="1" stop-color="#fff" stop-opacity="0.698"></stop>
                            </linearGradient>
                        </defs>
                        <g id="Group_3723" data-name="Group 3723" transform="translate(-181.82 -748.118)">
                            <g id="Group_3721" data-name="Group 3721" transform="translate(181.82 748.118)">
                                <ellipse id="Ellipse_461" data-name="Ellipse 461" cx="17.201" cy="17.193" rx="17.201" ry="17.193" transform="translate(0 1.615)" fill="url(#linear-gradient)"></ellipse>
                                <path id="Path_1395" data-name="Path 1395" d="M223.68,749.877l-1.126-1.127a2.145,2.145,0,0,0-1.526-.633h0a2.144,2.144,0,0,0-1.526.632L207.555,760.7l-4.69-4.69a2.157,2.157,0,0,0-3.052,0l-1.127,1.126a2.161,2.161,0,0,0,0,3.052l7.343,7.342a2.16,2.16,0,0,0,3.052,0l14.6-14.6A2.16,2.16,0,0,0,223.68,749.877Z" transform="translate(-190.888 -748.118)" fill="#fff"></path>
                            </g>
                            <g id="Group_3722" data-name="Group 3722" transform="translate(226.728 753.241)">
                                <path id="Path_1396" data-name="Path 1396" d="M402.235,777.512a8.4,8.4,0,0,0-4.135-5.8,21.943,21.943,0,0,0-2.91-1.417c-2.129-.914-4.33-1.859-4.645-3.455a2.346,2.346,0,0,1,.422-2.011,3.491,3.491,0,0,1,2.738-1.069,4.845,4.845,0,0,1,3.892,2.056,1.085,1.085,0,0,0,1.5.148l1.459-1.165a1.082,1.082,0,0,0,.158-1.537,8.751,8.751,0,0,0-7.013-3.535,7.478,7.478,0,0,0-5.857,2.542,6.334,6.334,0,0,0-1.263,5.353c.728,3.681,4.212,5.177,7.014,6.38a19.266,19.266,0,0,1,2.367,1.133,4.51,4.51,0,0,1,2.269,2.917,2.591,2.591,0,0,1-.8,2.133,4.15,4.15,0,0,1-3.6,1.275,7.314,7.314,0,0,1-4.913-2.949,1.08,1.08,0,0,0-1.43-.292l-1.609.963a1.086,1.086,0,0,0-.33,1.557,11.279,11.279,0,0,0,7.866,4.734,9.45,9.45,0,0,0,.962.05,8.1,8.1,0,0,0,6.017-2.6A6.521,6.521,0,0,0,402.235,777.512Z" transform="translate(-340.513 -759.723)" fill="#fff"></path>
                                <path id="Path_1397" data-name="Path 1397" d="M340.2,760.306a.812.812,0,0,0-.76-.5h0a.812.812,0,0,0-.76.5l-9.947,23.67a.91.91,0,0,0,.839,1.262h2.058a1.426,1.426,0,0,0,1.318-.875l1.872-4.454h9.24l1.872,4.454a1.427,1.427,0,0,0,1.318.875h2.057a.91.91,0,0,0,.84-1.262Zm2.165,15.569h-5.85l2.925-6.96Z" transform="translate(-308.85 -759.766)" fill="#fff"></path>
                                <path id="Path_1398" data-name="Path 1398" d="M636.188,763.077c-2.718-2.686-6.864-2.637-8.85-2.615l-4.752,0a1.085,1.085,0,0,0-1.083,1.083v22.975a1.084,1.084,0,0,0,1.083,1.083h1.87a1.084,1.084,0,0,0,1.084-1.083v-6.235l1.794,0c1.989.025,6.131.075,8.853-2.616a9.645,9.645,0,0,0,0-12.6Zm-1.7,6.3a4.593,4.593,0,0,1-1.141,3.43c-1.518,1.5-4.41,1.468-5.973,1.45l-1.837,0V764.5l1.841,0c1.557-.019,4.451-.05,5.969,1.45A4.6,4.6,0,0,1,634.491,769.376Z" transform="translate(-472.414 -760.133)" fill="#fff"></path>
                                <path id="Path_1399" data-name="Path 1399" d="M301.255,760.457H284.864a1.085,1.085,0,0,0-1.084,1.083v1.868a1.084,1.084,0,0,0,1.084,1.083h6.177v20.031a1.084,1.084,0,0,0,1.084,1.083h1.87a1.084,1.084,0,0,0,1.084-1.083V764.491h6.177a1.084,1.084,0,0,0,1.084-1.083V761.54A1.085,1.085,0,0,0,301.255,760.457Z" transform="translate(-283.78 -760.133)" fill="#fff"></path>
                                <path id="Path_1400" data-name="Path 1400" d="M518.473,769.376c0-3.218-1.267-8.655-9.754-8.907-.533-.017-1.017-.011-1.438-.007l-3.662,0h-1.088a1.084,1.084,0,0,0-1.084,1.083v22.974a1.085,1.085,0,0,0,1.084,1.083h1.87a1.085,1.085,0,0,0,1.084-1.083v-6.233l1.793,0c.2,0,.411,0,.644,0,.217,0,.449,0,.691-.009l4.852,6.726a1.434,1.434,0,0,0,1.16.593h2.635a.824.824,0,0,0,.668-1.305l-4.874-6.756C517.665,775.9,518.473,771.938,518.473,769.376Zm-4.037,0c0,3.339-1.691,4.753-5.836,4.876-.462.014-.88.009-1.231.005l-1.884,0V764.5h1.885c.35,0,.769-.008,1.23,0C512.745,764.624,514.435,766.036,514.435,769.376Z" transform="translate(-405.357 -760.133)" fill="#fff"></path>
                                <path id="Path_1401" data-name="Path 1401" d="M563.674,759.723a12.9,12.9,0,1,0,12.9,12.9A12.916,12.916,0,0,0,563.674,759.723Zm8.866,12.9a8.866,8.866,0,1,1-8.866-8.865A8.875,8.875,0,0,1,572.54,772.621Z" transform="translate(-432.907 -759.723)" fill="#fff"></path>
                                <path id="Path_1402" data-name="Path 1402" d="M457.172,779.616l-1.466-1.156a1.085,1.085,0,0,0-1.469.121,8.881,8.881,0,0,1-6.546,2.9,8.875,8.875,0,0,1-8.853-9.37,8.9,8.9,0,0,1,8.63-8.355,8.809,8.809,0,0,1,6.768,2.9,1.085,1.085,0,0,0,1.47.121l1.465-1.156a1.088,1.088,0,0,0,.137-1.578,12.9,12.9,0,1,0,0,17.144,1.087,1.087,0,0,0-.139-1.575Z" transform="translate(-368.125 -759.722)" fill="#fff"></path>
                            </g>
                        </g>
                    </svg>

                </a>
                <h2 class="during-trial-period">試用期間中です</h2>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbars04" aria-controls="navbars04" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbars04">
                    <ul class="navbar-nav me-auto mb-2 mb-md-0"></ul>
                    <div class="d-flex wrap-right-nav">
                        <a href="http://localhost:8000/trash" class="nav-right-svg icon-delete__js">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 36 36">
                                <g id="Group_1445" data-name="Group 1445" transform="translate(-6009 1317)">
                                    <circle id="Ellipse_291" class="icon-bg-svg" data-name="Ellipse 291" cx="18" cy="18" r="18" transform="translate(6009 -1317)" fill="#6c757d"></circle>
                                    <g id="Component_9_59" data-name="Component 9 – 59" transform="translate(6018 -1309)">
                                        <path id="Path_843" class="icon-content-svg" data-name="Path 843" d="M21.5,6a1,1,0,0,1-.883.993L20.5,7h-.845L18.424,19.52A2.75,2.75,0,0,1,15.687,22H8.313a2.75,2.75,0,0,1-2.737-2.48L4.345,7H3.5a1,1,0,1,1,0-2h5a3.5,3.5,0,1,1,7,0h5a1,1,0,0,1,1,1ZM14.25,9.25a.75.75,0,0,0-.743.648L13.5,10v7l.007.1a.75.75,0,0,0,1.486,0L15,17V10l-.007-.1a.75.75,0,0,0-.743-.648Zm-4.5,0a.75.75,0,0,0-.743.648L9,10v7l.007.1a.75.75,0,0,0,1.486,0L10.5,17V10l-.007-.1A.75.75,0,0,0,9.75,9.25ZM12,3.5A1.5,1.5,0,0,0,10.5,5h3A1.5,1.5,0,0,0,12,3.5Z" transform="translate(-2.5 -1.5)" fill="#f8f9fa"></path>
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <div class="nav-notification nav-right-svg">
                            <div class="position-relative" style="z-index: 9">
                                <span class="has-notification position-absolute" style="display: block;"></span>
                            </div>
                            <svg class="position-relative dropdown-toggle" tabindex="0" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="false" id="Icon_-_Bell_-_White" data-name="Icon - Bell - White" xmlns="http://www.w3.org/2000/svg" width="32" height="35" viewBox="0 0 36 36">
                                <path id="Path_1" class="icon-no-bg-svg" data-name="Path 1" d="M33.845,31.5H22.537a4.523,4.523,0,0,1-9.046,0H2.183A2.17,2.17,0,0,1,.147,29.925,2.394,2.394,0,0,1,.826,27.45a9.043,9.043,0,0,0,3.619-7.2V13.5a13.57,13.57,0,0,1,27.139,0v6.75a9.043,9.043,0,0,0,3.619,7.2,2.124,2.124,0,0,1,.678,2.475A2.17,2.17,0,0,1,33.845,31.5Z" transform="translate(-0.063)" fill="#6c757d" fill-rule="evenodd"></path>
                            </svg>
                            <div class="dropdown-notification dropdown-menu ">
                                <input type="text" hidden="" class="user-read-notification" id="http://localhost:8000/user-read-notification">
                                <input type="text" hidden="" class="delete-user-notification" id="http://localhost:8000/delete-user-notification">
                                <input type="text" hidden="" class="update-all-flag-notification" id="http://localhost:8000/update-all-flag-notification">
                                <input type="text" hidden="" class="get-notification-by-user" id="http://localhost:8000/get-notification-by-user">
                                <input type="text" hidden="" class="message-no-data" value="データがありません">
                                <input type="text" hidden="" class="get-list-color-code" value="http://localhost:8000/get-list-color-code">

                                <div class="read-all">
                                    全て既読する
                                </div>
                                <div class="wrap-notification">

                                    <div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="CAs6pTl1-3gpZ-dctQ-TLpe-KV3mS3BcWOng">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>Project 原田 陽一 Add member kazuya.yamada</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"><a href="http://localhost/project/task-group/D5R5ZAJt-2Uay-wUAs-ImyJ-kmUku8kybbrP">Project 原田 陽一</a></p>
                                                <span class="date">2022/06/29 13:06</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="W0egfnTX-dght-i6Nc-wm5p-t7iO3Xknwgxs">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>Project 高橋 裕美子 Add member kazuya.yamada</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"><a href="http://localhost/project/task-group/ct6XpEIH-YDXS-z01b-8Z0I-3nSbWPDqRUMQ">Project 高橋 裕美子</a></p>
                                                <span class="date">2022/06/02 11:42</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="22uKhKgx-zYRC-H3nK-UX0n-gs9HfNIYIcun">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>hamada.kaori send contact reply</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="BaHhhez2-hMPb-u20Z-kmhY-3yrISXuH00MO">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>msasaki send contact request</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="HOyhnSGg-qdyA-BqrS-mm6c-cOtFDtF2rKRu">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>ryosuke.matsumoto send contact reply</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="qJt2XU4q-AuWf-T0xl-M3uP-dbTxpMTQ2R8v">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>miyazawa.yoko send contact reply</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="uAG54YBB-QGFZ-avxp-U5LC-P9gFCFnL5KVP">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>shota.yoshimoto send contact request</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="9hKsegBm-iM3J-e8Z2-YGMy-6mL4JZW7XyZK">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>fnakamura send contact reply</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="EHMnL6wV-ehbB-OuP9-BwYJ-rcIbxXo7QcLX">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>satomi12 send contact reply</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="OpkaE29B-ndtF-qZnn-5PxI-pAP0xEFrlZzD">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>idaka.tsubasa send contact request</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="RvWhqlYl-gYbn-U9cQ-PtzQ-lClvUJZyamX5">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>suzuki.naoto send contact request</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="16rsUTrv-TnGb-Phg7-8eUN-gVXsey2ibPwu">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>idaka.tsubasa send contact request</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"></p>
                                                <span class="date">2022/05/18 10:37</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="ocLXevWC-KVEP-d8ce-pbH0-TmYAW0LRIIz6">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>Project 杉山 直樹 Add member kazuya.yamada</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"><a href="http://localhost/project/task-group/9G9IgPLJ-2Ym6-RcCD-EODt-x4rvmsi3s7vU">Project 杉山 直樹</a></p>
                                                <span class="date">2022/05/15 05:55</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="18IQuT4k-IMGV-3iCm-x1ni-FWKAUL6eu9wq">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>Project 坂本 春香 Add member kazuya.yamada</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"><a href="http://localhost/project/task-group/rVQypB4e-dKTB-CGuf-x7cX-jB2HcwDkVA3u">Project 坂本 春香</a></p>
                                                <span class="date">2022/04/07 22:56</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="DO98vmYq-l9II-x0hR-HUQo-ZKQJqiCnmQ7n">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>Project 坂本 直人 Add member kazuya.yamada</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"><a href="http://localhost/project/task-group/L764Evbf-NDb3-JZ0w-ZBef-Afgakd85DPta">Project 坂本 直人</a></p>
                                                <span class="date">2022/03/29 10:08</span>
                                            </div>
                                        </div>
                                    </div><div class="box-content-notification" data-kind-notice="0">
                                        <div class="box-content-wp" id="IbtLXIaA-sMda-fC8U-RBhQ-5GFUO24BVKqq">
                                            <div class="svg-wp d-flex justify-content-between">
                                                <svg class="status-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 12 12">
                                                    <circle id="Ellipse_309" data-name="Ellipse 309" cx="6" cy="6" r="6" fill="#ecad42"></circle>
                                                </svg>
                                                <svg class="btn-close-notification" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 10 10">
                                                    <path id="Union_8" data-name="Union 8" d="M5,5.909.909,10,0,9.091,4.091,5,0,.909.909,0,5,4.091,9.091,0,10,.909,5.909,5,10,9.091,9.091,10Z" fill="#7c7c7c"></path>
                                                </svg>
                                            </div>
                                            <div class="bc-action d-flex align-items-center">
                                                <div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26.744" height="28" viewBox="0 0 26.744 28">
                                                        <defs>
                                                            <clipPath id="clip-path">
                                                                <path id="Path_1463" data-name="Path 1463" d="M0,16.3A13.372,13.372,0,1,0,13.372,2.926,13.372,13.372,0,0,0,0,16.3" transform="translate(0 -2.926)" fill="none"></path>
                                                            </clipPath>
                                                            <linearGradient id="linear-gradient" x1="0.134" y1="0.866" x2="0.146" y2="0.866" gradientUnits="objectBoundingBox">
                                                                <stop offset="0" stop-color="#f36a10"></stop>
                                                                <stop offset="1" stop-color="#ffa800"></stop>
                                                            </linearGradient>
                                                            <clipPath id="clip-path-2">
                                                                <rect id="Rectangle_2586" data-name="Rectangle 2586" width="26.745" height="28" fill="none"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="Group_4777" data-name="Group 4777" transform="translate(0 0)">
                                                            <g id="Group_4774" data-name="Group 4774" transform="translate(0 1.255)">
                                                                <g id="Group_4773" data-name="Group 4773" clip-path="url(#clip-path)">
                                                                    <rect id="Rectangle_2585" data-name="Rectangle 2585" width="36.534" height="36.534" transform="matrix(0.5, -0.866, 0.866, 0.5, -11.581, 20.059)" fill="#ffa800"></rect>
                                                                </g>
                                                            </g>
                                                            <g id="Group_4776" data-name="Group 4776" transform="translate(0 0)">
                                                                <g id="Group_4775" data-name="Group 4775" clip-path="url(#clip-path-2)">
                                                                    <path id="Path_1464" data-name="Path 1464" d="M32.909,1.368,32.033.492a1.677,1.677,0,0,0-2.373,0L20.374,9.779,16.728,6.133a1.678,1.678,0,0,0-2.373,0l-.876.876a1.68,1.68,0,0,0,0,2.373l5.708,5.708a1.679,1.679,0,0,0,2.373,0L32.909,3.74a1.679,1.679,0,0,0,0-2.372" transform="translate(-7.417 0)" fill="#009164"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg></div>
                                                <span>Project 原田 春香 Add member kazuya.yamada</span>
                                            </div>
                                            <div class="bc-infor-project ">
                                                <p class="name-project"><a href="http://localhost/project/task-group/0RoHeiic-pRg1-UWvI-4tn3-THiWvB8qjnrk">Project 原田 春香</a></p>
                                                <span class="date">2022/03/24 04:44</span>
                                            </div>
                                        </div>
                                    </div></div>
                            </div>
                        </div>
                        <a href="http://localhost:8000/question-answer" class="nav-right-svg" id="icon-quection">
                            <svg id="Component_8_59" data-name="Component 8 – 59" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 36 36">
                                <circle id="Ellipse_228" class="icon-bg-svg" data-name="Ellipse 228" cx="18" cy="18" r="18" fill="#6c757d"></circle>
                                <text id="_" class="icon-content-svg" data-name="?" transform="translate(11 28.5)" fill="#fff" font-size="30" font-family="YuGothicUI-Bold, Yu Gothic UI" font-weight="700">
                                    <tspan x="0" y="0">?</tspan>
                                </text>
                            </svg>
                        </a>
                        <div class="nav-setting nav-right-svg">
                            <svg class=" dropdown-toggle" tabindex="0" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="false" id="Avatar" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 36 36">
                                <circle id="Ellipse_98" class="icon-bg-svg" data-name="Ellipse 98" cx="18" cy="18" r="18" transform="translate(0)" fill="#6c757d"></circle>
                                <g id="Icon_-_User" data-name="Icon - User" transform="translate(9.54 9.363)">
                                    <path id="Union_1" class="icon-content-svg" data-name="Union 1" d="M0,17.86V15.628c0-2.456,3.807-4.466,8.46-4.466s8.46,2.009,8.46,4.466V17.86ZM4.23,4.466A4.353,4.353,0,0,1,8.46,0a4.352,4.352,0,0,1,4.23,4.466A4.352,4.352,0,0,1,8.46,8.93,4.352,4.352,0,0,1,4.23,4.466Z" fill="#fff"></path>
                                </g>
                            </svg>
                            <div class="ribbon dropdown-menu ">
                                <div class="wrap-color">
                                    <a href="http://localhost:8000/users/user-setting" class="setting-account__link link ">
                                        アカウント設定
                                    </a>
                                    <hr>
                                    <div class="change-color">
                                        <a class="setting-color__link link" href="http://localhost:8000/develop/B010_%E3%83%97%E3%83%AD%E3%83%95%E3%82%A3%E3%83%BC%E3%83%AB%E8%A8%AD%E5%AE%9A">メインカラー設定</a>

                                        <div class="btn-list-color d-flex">

                                            <div class="color active change-color-js" id="dc672090-d65b-11ec-b76c-a35ef29a28db">
                                                <a class="color__link ">
                                                    <svg class="" xmlns="http://www.w3.org/2000/svg" width="36" height="38" viewBox="0 0 36 38">
                                                        <g id="Group_1665" data-name="Group 1665" transform="translate(-0.072 0.306)">
                                                            <rect id="Rectangle_1783" data-name="Rectangle 1783" width="36" height="35" rx="12" transform="translate(0.072 2.694)" fill="#d1d1d1"></rect>
                                                            <rect id="Rectangle_1788" data-name="Rectangle 1788" width="36" height="34" rx="12" transform="translate(0.072 -0.306)" fill="#14746f"></rect>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div> <div class="color  change-color-js" id="dc6721c0-d65b-11ec-b386-c701521d7239">
                                                <a class="color__link ">
                                                    <svg class="" xmlns="http://www.w3.org/2000/svg" width="36" height="38" viewBox="0 0 36 38">
                                                        <g id="Group_1665" data-name="Group 1665" transform="translate(-0.072 0.306)">
                                                            <rect id="Rectangle_1783" data-name="Rectangle 1783" width="36" height="35" rx="12" transform="translate(0.072 2.694)" fill="#d1d1d1"></rect>
                                                            <rect id="Rectangle_1788" data-name="Rectangle 1788" width="36" height="34" rx="12" transform="translate(0.072 -0.306)" fill="#bf6641"></rect>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div> <div class="color  change-color-js" id="dc672220-d65b-11ec-9545-ad7541263a9c">
                                                <a class="color__link ">
                                                    <svg class="" xmlns="http://www.w3.org/2000/svg" width="36" height="38" viewBox="0 0 36 38">
                                                        <g id="Group_1665" data-name="Group 1665" transform="translate(-0.072 0.306)">
                                                            <rect id="Rectangle_1783" data-name="Rectangle 1783" width="36" height="35" rx="12" transform="translate(0.072 2.694)" fill="#d1d1d1"></rect>
                                                            <rect id="Rectangle_1788" data-name="Rectangle 1788" width="36" height="34" rx="12" transform="translate(0.072 -0.306)" fill="#a55353"></rect>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div> <div class="color  change-color-js" id="dc672270-d65b-11ec-b912-7735a0a5c7bd">
                                                <a class="color__link ">
                                                    <svg class="" xmlns="http://www.w3.org/2000/svg" width="36" height="38" viewBox="0 0 36 38">
                                                        <g id="Group_1665" data-name="Group 1665" transform="translate(-0.072 0.306)">
                                                            <rect id="Rectangle_1783" data-name="Rectangle 1783" width="36" height="35" rx="12" transform="translate(0.072 2.694)" fill="#d1d1d1"></rect>
                                                            <rect id="Rectangle_1788" data-name="Rectangle 1788" width="36" height="34" rx="12" transform="translate(0.072 -0.306)" fill="#343a40"></rect>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div></div>
                                    </div>
                                    <hr>
                                    <a class="logout__link link" id="buttonLogout" onclick="javascript: document.getElementById('logoutForm').submit();">
                                        ログアウト
                                    </a>
                                    <form method="POST" id="logoutForm" action="http://localhost:8000/logout">
                                        <input type="hidden" name="_token" value="3NtJnriCjQHQ8JegZyefO6sGubFoSDSQCCkYn5lq">                            </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </nav>

        <div id="app">
            <notification-management></notification-management>
        </div>
        <script src="/js/app.js"></script>

        <script type="text/javascript">
            const tooltip_menu = "編集や削除を実行します。"; //編集や削除を実行します。
            const tooltip_like = "リアクションします。"; //リアクションします。
            const tooltip_delete_1 = "削除します。"; //削除します。
            const tooltip_delete_2 = "ゴミ箱へ移動します。"; //ゴミ箱へ移動します。
            const tooltip_copy = "タスクをコピーします。"; //タスクをコピーします。
            const tooltip_calendar = "タスクの期間"; //タスクの期間
            const tooltip_download = "ダウンロードします。"; //ダウンロードします。
            const tooltip_pdf = "PDFでダウンロードします。"; //PDFでダウンロードします。
            const tooltip_upload = "アップロードします。"; //アップロードします。
            const tooltip_group = "グループ"; //グループ
            const tooltip_task = "タスク"; //タスク
            const tooltip_subtask = "サブタスク"; //サブタスク
            const tooltip_stamp = "スタンプ"; //スタンプ
            const tooltip_sort = "ソートします。"; //ソートします。
            const tooltip_eye = "パスワードを表示します。"; //パスワードを表示します。
            const tooltip_file = "ファイル"; //ファイル
            const tooltip_font = "フォント"; //フォント
            const tooltip_select_item = "項目を選択します。"; //項目を選択します。
            const tooltip_select_date = "年月日を選択します。"; //年月日を選択します。
            const tooltip_project = "プロジェクト"; //プロジェクト
            const tooltip_user_name = "（ユーザ名？）"; //（ユーザ名？）
            const tooltip_show_personal = "個人設定を表示します。"; //個人設定を表示します。
            const tooltip_history = "履歴を表示します。"; //履歴を表示します。
            const tooltip_start = "開始します。"; //開始します。
            const tooltip_fold_bar = "バーを折りたたみます。"; //バーを折りたたみます。
            const tooltip_layout_display = "メニューバーを折りたたみます。"; //メニューバーを折りたたみます
            const tooltip_layout_hidden = "メニューバーを表示します。"; //メニューバーを表示します。
            const tooltip_task_complete_1 = "タスクが完了しました。"; //タスクが完了しました。
            const tooltip_task_complete_2 = "プロジェクトが完了しました。"; //プロジェクトが完了しました。
            const tooltip_search = "検索を実行します。"; //検索を実行します。
            const tooltip_input_search = "（検索対象についてメッセージを表示）"; //（検索対象についてメッセージを表示）
            const tooltip_correct_schedule = "順調です。"; //順調です。
            const tooltip_bit_slow_schedule = "遅れ気味です。"; //遅れ気味です。
            const tooltip_slow_schedule = "大幅に遅れています。"; //大幅に遅れています。
            const tooltip_send = "送信します。"; //送信します。
            const tooltip_add_1 = "項目を追加します。"; //項目を追加します。
            const tooltip_add_2 = "プロジェクトを追加します。"; //プロジェクトを追加します。
            const tooltip_notify = "通知を表示します。"; //通知を表示します。
            const tooltip_attach = "添付"; //添付
            const tooltip_edit = "編集します。"; //編集します。
            const tooltip_close_1 = "閉じる"; //閉じる
            const tooltip_close_2 = "削除します。"; //削除します。
            const tooltip_star = "ウォッチ"; //ウォッチ
            const tooltip_help_1 = "（ヘルプの説明文表示）"; //（ヘルプの説明文表示）
            const tooltip_help_2 = "ヘルプページを表示します。"; //ヘルプページを表示します。
            const tooltip_checklist = "チェックリスト"; //チェックリスト
            const tooltip_follow_up = "フォローアップをします。"; //フォローアップをします。
            const tooltip_question = "チェックを入れた内容がコピーされます。"; //チェックを入れた内容がコピーされます。
            const tooltip_company_search_keyword_2 = "カードに記載されている3もしくは4桁のコードを入力してください。"; //企業検索の際にこちらに登録したワードで自社がヒットするようになります。
            const tooltip_edit_project_information = "企業検索の際にこちらに登録したワードで自社がヒットするようになります。"; //企業検索の際にこちらに登録したワードで自社がヒットするようになります。
            const tooltip_contact = "チェックを入れた内容がテンプレート保存されます。"; //チェックを入れた属性がタスクにコピーされます。
            const tooltip_task_attributes = "チェックを入れた属性がタスクにコピーされます。"; //チェックを入れた内容がテンプレート保存されます。
            const BASE_PATH = 'http://localhost/storage/';

            // example of alternative callback
            var tribute = new Tribute({
                // menuContainer: document.getElementById('content'),
                values: [{
                    key: "Jordan Humphreys",
                    value: "Jordan Humphreys",
                    email: "getstarted@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                }, {
                    key: "Sir Walter Riley",
                    value: "Sir Walter Riley",
                    email: "getstarted+riley@zurb.com",
                }, {
                    key: "Joachim",
                    value: "Joachim",
                    email: "getstarted+joachim@zurb.com",
                },],
                selectTemplate: function (item) {
                    if (typeof item === "undefined") return null;
                    if (this.range.isContentEditable(this.current.element)) {
                        return (
                            '<span contenteditable="false"><a href="#" target="_blank" title="' +
                            item.original.email +
                            '">@' +
                            item.original.value +
                            "</a></span>"
                        );
                    }

                    return "@" + item.original.value;
                },
                // Limits the number of items in the menu
                menuItemLimit: 5,
                requireLeadingSpace: false,
            });
            tribute.attach(document.getElementsByClassName("sun-editor-editable"));
        </script>

        <script type="text/javascript" src="http://localhost:8000/js/change-color.js "></script>

        <!--XRAY END 8--><div class="modal-backdrop fade show"></div>
    </body>
</html>
