<?php

use App\Http\Controllers\API\NoticeKind\NoticeKindController;
use App\Http\Controllers\PC\Chat\ChatController;
use App\Http\Controllers\PC\HomeController;
use App\Http\Controllers\PC\Licence\LicenceController;
use App\Http\Controllers\PC\Payment\PaymentController;
use App\Http\Controllers\PC\User\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([ 'middleware' => ['web', 'locale', 'auth:web']], function () {
    Route::get('/', [HomeController::class, 'home'])->name('pc.home');
});

Route::get('/terms', function () {
    return view('terms.index');
})->name('pc.page.terms');
Route::get('/policy', function () {
    return view('policy.index');
})->name('pc.page.policy');
Route::get('/question-answer', function () {
    return view('question-answer.index');
})->name('pc.page.question-answer');
Route::get('/trash', function () {
    return view('trash.index');
})->name('pc.page.trash');
Route::get('/service/payment-management', function () {
    return view('service.payment-management.index');
})->name('pc.service.payment-management');
/**
 * Route auth
 */
require_once('auth_web.php');

/**
 * Route without login
 */
Route::group(['namespace' => 'PC', 'middleware' => ['web', 'locale']], function () {
    Route::get('change-language', 'HomeController@changeLanguage')->name('pc.change-language');
});

Route::post('reset-password-from-email', [UserController::class, 'resetPasswordFromEmail'])->name('pc.user.reset-password-from-email');

/**
 * Route requires login
 */
Route::group(['namespace' => 'PC', 'middleware' => ['auth:web', 'locale']], function () {
    Route::group(['middleware' => ['web', 'locale']], function () {
        Route::get('/users', 'User\UserController@index')->name('pc-users.index');
    });

    // add method payment
    Route::get('payments/manage-payment-method', [PaymentController::class, 'managePaymentMethodIndex'])->name('payment.add-payment-method');
    Route::get('payments/payment-intent-save-card', [PaymentController::class, 'paymentIntentSaveCard'])->name('payment.payment-intent-save-card');
    Route::get('payments/setup-intent-save-card', [PaymentController::class, 'setupIntentSaveCard'])->name('payment.setup-intent-save-card');
    Route::post('payments/delete-card', [PaymentController::class, 'deleteCard'])->name('pc.payment.delete-card');

    // licence

    Route::group(['prefix' => 'licences'], function () {
        Route::get('add-licence', [LicenceController::class, 'addLicenceIndex'])->name('pc.licence.add-licence-index');
        Route::get('delete-licence', [LicenceController::class, 'deleteLicenceIndex'])->name('pc.licence.delete-licence-index');
        Route::get('confirm-add-licence', [LicenceController::class, 'confirmAddLicenceIndex'])->name('pc.licence.confirm-add-licence-index');

        // Buy licence
        Route::post('buy-licence-ajax', [LicenceController::class, 'buyLicenceAjax'])->name('pc.licence.buy-licence-ajax');
        // delete licence
        Route::post('delete-licence-ajax', [LicenceController::class, 'deleteLicenceAjax'])->name('pc.licence.delete-licence-ajax');
    });

    // url develop
    Route::get('develop/{slug}', function (Request $request) {
        $slug = $request->slug;
        return view("errors.develop")->with('slug', $slug);
    })->name('pc.develop');

//    require_once('user_web.php');

    //demo chat
    Route::get('chat', [ChatController::class, 'index'])->name('pc.chat.index');
    Route::get('chat/load-message-ajax', [ChatController::class, 'getMesssageAjax'])->name('pc.chat.load-message-ajax');
    Route::post('chat/create-message', [ChatController::class, 'storeMessage'])->name('pc.chat.create-message');

    //test drop
    Route::get('drop', [ChatController::class, 'dropIndex'])->name('pc.drop.index');
});

Route::post('/stripe-event', [\App\Http\Controllers\StripeEventController::class, 'stripeEvent'])->name('stripe-event');
Route::get('/notify', [NoticeKindController::class, 'getListNoticeKinds'])->name('notify');

//require_once('company_web.php');
//require_once('url_verify_web.php');
//require_once('project_web.php');
//require_once('user_register_web.php');
//require_once('user_task_web.php');
//require_once('user_project_web.php');
//require_once('user_notification.php');
//require_once('trash_web.php');
//require_once('role_web.php');
//require_once('project_notification_web.php');
//require_once('contact_web.php');
//require_once('attachment_file_web.php');
//require_once('task_web.php');
//require_once('breakdown_web.php');
//require_once('contract_web.php');
//require_once('task_group_web.php');
//require_once('user_group_web.php');
